var SERVER_PORT = 1234, // Bind port
    SERVER_ADDRESS = "0.0.0.0", // Bind IP
    MAX_TOTAL_TABLES = 15, // Maximum number of tables (15 = 30 players)
    LOGLEVEL = 0,
    BUZZLIMIT = 15; // Time between buzzes.

//  No need to edit below this line, unless you know what you're doing.

var http = require('http'),
    url = require('url'),
    fs = require('fs'),
    path = require('path'),
    tables = {};
console.log('Starting RPS server.');
var server = http.createServer(function(req, res) { 
    var request = url.parse(req.url,true);
    request.pathname = request.pathname.replace(/\.\.?\//g,'');
    var fullPath = __dirname+'/content/'+request.pathname;
    fs.exists(fullPath,function (exists) {
       if (exists) {
          res.writeHead(200,{'Content-Type':(fullPath.indexOf('.ogg')!==-1?'audio/ogg':'text/html')});
          if (fs.statSync(fullPath).isDirectory()) {
             var index = path.join(fullPath,'index.html');
             if (fs.existsSync(index)) {
                fullPath = index;
              } else {
                fullPath = false;
                res.end('ERR: NO INDEX');     
             }
          }
          if (fullPath) {
             res.fstream = fs.createReadStream(fullPath,{
                              flags: 'r',
                              mode: 0666
                         }).on('data',function(chunk){
                               // res.write(chunk); /* Do nothing with the chunk. (for now?) */
                         }).on('end',function(){
                               this.end();
                         }.bind(res)).on('error',function(err){
                               // Send an error...
                         }.bind(this)).pipe(res);
          }
        } else {
          res.writeHead(400,{'Content-Type':'text/html'});
          res.end('404 - File Not Found');
       }
    });
});
server.listen(SERVER_PORT,SERVER_ADDRESS||'0.0.0.0');
var socket = require('socket.io').listen(server);
socket.set('transports', ['websocket','flashsocket','jsonp-polling']);
socket.set('log level',LOGLEVEL||0);

/* RPS server */
socket.of('/rps').on('connection',function(client) { 
  cleanObject(tables);
  if (tables.length >= MAX_TOTAL_TABLES) {
     client.emit('fatal_error',{content:'Sorry, the server is currently at maximum capacity. Please try again later. (Maximum users: '+MAX_TOTAL_TABLES+')'}); return false;
  }
 
  client.emit('connected',{id:client.id});

  client.on('request_table',function (event) {
        if (!event.table || event.table == "") {
           event.table = randomID(20);
        }
        if (checkTable(event.table,client)) {
           return false;
        }
        if (isTable(event.table)) {
           if (tables[event.table]['users'] >= 2) {
              client.emit('table_full',{content:'Sorry, this table is full. Maybe you should {{CREATE}}?'}); return false;
           }
           client.join(event.table);
           client.store.store.table = event.table;
           tables[event.table]['player_2'] = client.id;
           tables[event.table]['users'] += 1;
           client.emit('connected_to_table',{table:event.table,'player':2,users:2});
           client.broadcast.to(event.table).emit('table_ready',{});
         } else {
           client.join(event.table);
           client.store.store.table = event.table;
           tables[event.table] = {'name':event.table,'users':1,'player_1':client.id,'player_2':null,table:event.table,chosen:{1:false,2:false},scores:{1:0,2:0,ties:0}};
           tables[event.table]['buzz'] = {};
           client.emit('connected_to_table',{table:event.table,'player':1,users:1});
        }
  });

  client.on('chosen',function (event) {
        if (checkTable(event.table,client)) {
           return false;
        }
        if (tables[event.table]) {
           if (client.id == tables[event.table]['player_1']) {
              tables[event.table]['chosen'][1] = event.move;
            } else {
              tables[event.table]['chosen'][2] = event.move;
           }
           if (tables[event.table]['chosen'][1] && tables[event.table]['chosen'][2]) {
              var winner = getWinner(tables[event.table]['chosen'][1],tables[event.table]['chosen'][2]);
              if (winner == 1) {
                 tables[event.table]['scores'][1] += 1;
              } else if (winner == 2) {
                 tables[event.table]['scores'][2] += 1;
              } else {
                 tables[event.table]['scores']['ties'] += 1;
              }
              var send = {p:winner,P1:tables[event.table]['scores'][1],P2:tables[event.table]['scores'][2],ties:tables[event.table]['scores']['ties']};
              client.emit('winner',send);
              client.broadcast.to(event.table).emit('winner',send);
              tables[event.table]['chosen'] = {1:false,2:false};
            } else {
              client.broadcast.to(event.table).emit('chosen',{content:'Other player is waiting on you.'});
           }
        }
  });

  client.on('new_table',function (event) {
        if (checkTable(event.table,client)) {
           return false;
        }
        if (isPlayer(client.id,event.table)) {
           client.broadcast.to(event.table).emit('player_left',{});
           delete tables[event.table];
        }
        client.emit('get_new_table',{});
  });

  client.on('disconnect',function(){
        var client_table = client.store.store.table;
        if (isPlayer(client.id,client_table)) {
           client.broadcast.to(client_table).emit('player_left',{});
           delete tables[client_table];
        }
  });

  client.on('leave_table',function (event) {
        if (checkTable(event.table,client)) {
           return false;
        }
        if (isPlayer(client.id,event.table)) {
           client.broadcast.to(event.table).emit('player_left',{});
           delete tables[event.table];
        }
  });

  client.on('chat-out',function (event) {
        if (checkTable(event.table,client)) {
           return false;
        }
        var msg = event.msg.toString().replace(/[^a-zA-Z0-9!.?:=\s]/g,"");
        if (!msg) {
           return;
        }
        if (isPlayer(client.id,event.table)) {
           client.broadcast.to(event.table).emit('chat-in',{msg:event.msg});
        }
  });

  client.on('send-alert',function (event) {
        if (checkTable(event.table,client)) {
           return false;
        }
        if (isPlayer(client.id,event.table)) {
           var time = Math.floor((new Date().getTime()/1000));
           if (!tables[event.table]['buzz'][client.id]) {
              tables[event.table]['buzz'][client.id] = 1;
           }
           if (tables[event.table]['buzz'][client.id] < time-BUZZLIMIT) {
              client.broadcast.to(event.table).emit('play_alert',{play:true});
              client.emit('played_alert',{played:true,limit:BUZZLIMIT});
              tables[event.table]['buzz'][client.id] = time;
            } else {
              client.emit('played_alert',{played:false,limit:BUZZLIMIT});
           }
        }
  });

  client.on('join_open_table',function () {
        var tbls = [];
        for (var key in tables) {
            tbls.push(key);
        }
        tbls.sort(function(a,b){return (Math.round(Math.random())-0.5)});
        for (var i = 0; i < tbls.length; i++) {
            var key = tbls[i];
            if (tables[key] && tables[key]['users'] <= 1 && !('private' in tables[key])) {
               client.emit('open_table',{table:key});
               return false;
            }
        }
        client.emit('open_table',{table:null});
  });
  
  client.on('set_private',function (event) {
        if (checkTable(event.table,client)) {
           return false;
        }
        if (isPlayer(client.id,event.table)) {
           tables[event.table]['private'] = true;
        }
  });
});

function isPlayer(id,table) {
         if (isTable(table)) {
            if (tables[table]['player_1'] == id || tables[table]['player_2'] == id) {
               return true;
            }
         }
         return false;
}
function isTable (name) {
         for (var id in tables) {
             if (tables[id].name == name) {
                return true;
             }
         }
         return false;
}
function getWinner (p1,p2) {
         return (p1==p2?0:(p1 == 'r' && p2 == 's' || p1 == 's' && p2 == 'p' || p1 == 'p' && p2 == 'r'?1:2));
}
function cleanObject (obj) {
         for (var i in obj) {
             if (obj[i] === null || obj[i] === undefined) {
                delete obj[i];
             }
         }
}
function randomID (L) {
         var s= '';
         var randomchar = function () {
             var n = Math.floor(Math.random()*62);
             if (n < 10) { return n; }
             if (n < 36) { return String.fromCharCode(n+55); }
             return String.fromCharCode(n+61);
         }
         while (s.length < L) { s+= randomchar(); }
         return s;
}
function checkTable (table,client) {
        if (typeof(table) !== 'string') {
           table = table.toString();
        }
        var chk = table.replace(/[^a-zA-Z0-9]/g,"");
        if (!chk) {
           client.emit('fatal_error',{content:'Invalid table name.'}); return false;
        }
}
